# This module is based upon the example code available at:
#  https://albumentations.ai/docs/autoalbument/examples/pascal_voc/
# and also at:
#  https://github.com/albumentations-team/autoalbument/blob/master/examples/pascal_voc/dataset.py
#
# The code in that repository was licensed under the MIT License:
#  https://github.com/albumentations-team/autoalbument/blob/master/LICENSE
# """
# MIT License
# 
# Copyright (c) 2020  Alex Parinov, Vladimir Iglovikov, Evegene Khvedchenya,
# Mikhail Druzhinin, Buslaev Alexander.
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# """
#
# James Boyden does NOT claim ownership over the code in this Python file
# for the purposes of copyright (despite significant modifications from the
# original example code).

import cv2
from numpy import all as np_all
from numpy import dstack as np_dstack
from numpy import float32 as np_float32
from numpy import newaxis as np_newaxis
from numpy import vstack as np_vstack
from numpy import zeros as np_zeros
from torchvision.datasets import VOCSegmentation


VOC_CLASSES = [
    "background",
    "aeroplane",
    "bicycle",
    "bird",
    "boat",
    "bottle",
    "bus",
    "car",
    "cat",
    "chair",
    "cow",
    "diningtable",
    "dog",
    "horse",
    "motorbike",
    "person",
    "potted plant",
    "sheep",
    "sofa",
    "train",
    "tv/monitor",
]


VOC_COLORMAP = [
    [0, 0, 0],
    [128, 0, 0],
    [0, 128, 0],
    [128, 128, 0],
    [0, 0, 128],
    [128, 0, 128],
    [0, 128, 128],
    [128, 128, 128],
    [64, 0, 0],
    [192, 0, 0],
    [64, 128, 0],
    [192, 128, 0],
    [64, 0, 128],
    [192, 0, 128],
    [64, 128, 128],
    [192, 128, 128],
    [0, 64, 0],
    [128, 64, 0],
    [0, 192, 0],
    [128, 192, 0],
    [0, 64, 128],
]


# The AutoAlbument example code defines a class `PascalVOCSearchDataset`,
# which in turn extends the TorchVision class `VOCSegmentation`:
#  https://pytorch.org/vision/main/generated/torchvision.datasets.VOCSegmentation.html
#   """
#   Parameters:
#    * root (string) – Root directory of the VOC Dataset.
#    * year (string, optional) – The dataset year,
#                                supports years `"2007"` to `"2012"`.
#    * image_set (string, optional) – Select the image_set to use,
#                                     `"train"`, `"trainval"` or `"val"`.
#                                     If `year=="2007"`, can also be `"test"`.
#    * download (bool, optional) – If true, downloads the dataset from the
#                                  internet and puts it in root directory.
#                                  If dataset is already downloaded,
#                                  it is not downloaded again.
#    * transform (callable, optional) – A function/transform that takes in
#                                       a PIL image and returns a transformed
#                                       version.
#                                       E.g, `transforms.RandomCrop`
#    * target_transform (callable, optional) – A function/transform that takes
#                                              in the target and transforms it.
#    * transforms (callable, optional) – A function/transform that takes
#                                        input sample and its target as entry
#                                        and returns a transformed version.
#   """
#
# I've added these optional parameters to the `PascalVOCSearchDataset.__init__`
# method in the original code:
#  - `year="2012"` (passed into the `VOCSegmentation.__init__` method)
#  - `return_mask_axes_HWC=False` (see long comment in `__init__` method)

class PascalVOCSearchDataset(VOCSegmentation):
    def __init__(self,
            root="data/pascal_voc",
            year="2012",
            image_set="train",
            download=True,
            transform=None,
            return_mask_axes_HWC=False,
        ):
        super().__init__(root=root,
                year=year, image_set=image_set,
                download=download, transform=transform)

        self.return_mask_axes_HWC = return_mask_axes_HWC
        # Explanation for (^) this attribute `self.return_mask_axes_HWC`:
        #
        # We denote a shape (height, width, num_classes) as "HWC" for short,
        # following the practice of the Albumentations API docs:
        #  https://albumentations.ai/docs/api_reference/pytorch/transforms/
        #   """
        #   Convert image and mask to `torch.Tensor`. The numpy `HWC` image is
        #   converted to pytorch `CHW` tensor. If the image is in `HW` format
        #   (grayscale image), it will be converted to pytorch `HW` tensor. 
        #   <snip>
        #
        #   Parameters:
        #    - Name:        `transpose_mask`
        #    - Type:        `bool`
        #    - Description: If True and an input mask has three dimensions,
        #                   this transform will transpose dimensions so the
        #                   shape `[height, width, num_channels]` becomes
        #                   `[num_channels, height, width]`. The latter
        #                   format is a standard format for PyTorch Tensors.
        #                   Default: False.
        #   """
        #
        # PyTorch expects tensors to have shape (num_classes, height, width)
        # (or "CHW" for short).  For an example of PyTorch expecting "CHW",
        # consult the docs for `nn.CrossEntropyLoss`:
        #  https://pytorch.org/docs/stable/generated/torch.nn.CrossEntropyLoss.html
        #   """
        #   This criterion computes the cross entropy loss between input logits
        #   and target.
        #
        #   It is useful when training a classification problem with C classes.
        #   <snip>
        #
        #   The `input` is expected to contain the unnormalized logits for each
        #   class (which do *not* need to be positive or sum to 1, in general).
        #   `input` has to be a Tensor of size `(C)` for unbatched input,
        #   `(minibatch, C)` or `(minibatch, C, d_1, d_2, ..., d_K)` with
        #   `K >= 1` for the K-dimensional case. The last being useful for
        #   higher dimension inputs, such as computing cross entropy loss
        #   per-pixel for 2D images.
        #   """
        #
        # But as explained in function `_convert_to_segmentation_masks` above,
        # the AutoAlbument feature expects shape (height, width, num_classes)
        # (or "HWC" for short).
        #
        # If you specify the wrong value for `return_mask_axes_HWC`,
        # the mask will become batched `targets` of the wrong shape
        # (in comparison to the batched input `x` and the batched logits
        # `logits` calculated from `x`) in the model's `forward` method,
        # for example:
        #
        #       x.shape: torch.Size([16, 3, 200, 200])
        #       targets.shape: torch.Size([16, 200, 200, 21])
        #       logits.shape: torch.Size([16, 21, 200, 200])
        #
        # Observe that tensors `x` and `logits` are in the "CHW" format
        # expected by PyTorch, while `targets` is in the "HWC" format expected
        # by AutoAlbument (and originally computed by the code in this module).
        #
        # When the shape-mis-matched tensors `logits` and `targets` are passed
        # to the PyTorch cross-entropy loss function, the following exception
        # will be raised:
        #
        #   -> 3029     return torch._C._nn.cross_entropy_loss(input, target,
        #               weight, _Reduction.get_enum(reduction), ignore_index,
        #               label_smoothing)
        #
        #   RuntimeError: only batches of spatial targets supported (3D tensors)
        #   but got targets of dimension: 4
        #
        # (Ask me how I know this!)


    @staticmethod
    def _convert_to_segmentation_masks(mask):
        # It appears that the code in this module was originally created by
        # Albumentations to prepare inputs for their AutoAlbument feature.
        #
        # The following is the original comment in this function
        # (but edited with line-breaks for readability):
        #
        #       This function converts a mask from the Pascal VOC format to
        #       the format required by either PyTorch or AutoAlbument.
        #
        #       Pascal VOC uses an RGB image to encode the segmentation mask
        #       for that image. RGB values of a pixel encode the pixel's class.
        #
        #       AutoAlbument requires a segmentation mask to be a NumPy array
        #       with the shape [height, width, num_classes]. Each channel
        #       in this mask should encode values for a single class.
        #       Pixel in a mask channel should have a value of 1.0
        #       if the pixel of the image belongs to this class and
        #       0.0 otherwise.
        mask_shape_2d = tuple(mask.shape[:2])

        segmentation_masks = []
        for label_index, label in enumerate(VOC_COLORMAP):
            mask_for_label = np_all(mask == label, axis=-1).astype(np_float32)
            assert mask_for_label.shape == mask_shape_2d
            segmentation_masks.append(mask_for_label)

        return segmentation_masks


    @staticmethod
    def _recombine_segmentation_masks(masks, return_mask_axes_HWC):
        # The following switch `if return_mask_axes_HWC:` controls whether
        # this code should return the masks in the "HWC" or "CHW" format.
        if return_mask_axes_HWC:
            # The format that is expected by # AutoAlbument:
            # a Numpy array of shape (height, width, num_classes)
            # (or "HWC" for short)
            combined_mask_3d = np_dstack(masks)
            assert combined_mask_3d.shape[-1] == len(masks)
        else:
            # The format that is expected by PyTorch for tensors:
            # shape (num_classes, height, width) (or "CHW" for short)
            combined_mask_3d = np_vstack([
                    m[np_newaxis, :, :] for m in masks])
            assert combined_mask_3d.shape[0] == len(masks)

        return combined_mask_3d


    def __getitem__(self, index):
        image = cv2.imread(self.images[index])
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

        mask = cv2.imread(self.masks[index])
        mask = cv2.cvtColor(mask, cv2.COLOR_BGR2RGB)
        # Note that you need to convert the PASCAL VOC segmentation "mask"
        # *before* you apply any transforms.
        #
        # This PASCAL VOC segmentation "mask" is actually a colour image
        # of semantic labels.  We detect those semantic labels by checking
        # colour equality against the known colours in `VOC_COLORMAP`.
        # But a transform such as image resize might blur/blend colours,
        # which would prevent colour equality.
        masks = self._convert_to_segmentation_masks(mask)
        assert len(masks) == len(VOC_CLASSES)

        if self.transform is not None:
            transformed = self.transform(image=image, masks=masks)
            image = transformed["image"]
            masks = transformed["masks"]

        assert len(masks) == len(VOC_CLASSES)
        mask = self._recombine_segmentation_masks(masks,
                self.return_mask_axes_HWC)

        return (image, mask)
