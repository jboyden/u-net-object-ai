"""A `main` function to train a `U_Net` using PASCAL VOC segmentation dataset.

Copyright (c) 2023 James Boyden
"""

import torch
import pytorch_lightning as pl
import albumentations as A

import conf

from pytorch_lightning.loggers import TensorBoardLogger

from cmdline import get_argument_parser
from dataset import PascalVocSegDataModule
from model import U_Net


def main():
    # Create and populate the command-line argument parser.
    parser = get_argument_parser(
            descr='Train a U-Net model using PASCAL VOC segmentation dataset')

    PascalVocSegDataModule.add_dataset_specific_args(parser)
    U_Net.add_model_specific_args(parser)
    _add_trainer_specific_args(parser)

    # Extract (parse) the command-line arguments from the parser.
    parsed_args = parser.parse_args()

    # If we're doing a short run or we're overfitting batches,
    # we do NOT want to shuffle during training.
    shuffle_train = not(
            parsed_args.do_short_run or parsed_args.do_overfit_batches)

    # Instantiate classes, making use of specified command-line arguments.
    data = PascalVocSegDataModule(
            data_dir=parsed_args.data_dir,
            batch_size=parsed_args.batch_size,
            num_workers=parsed_args.num_workers,
            resize_image_H_W=parsed_args.resize_image_H_W,
            shuffle_train=shuffle_train,
            prior_train_transforms=[
                    A.Rotate(limit=30, p=1.0),
                    A.HorizontalFlip(p=0.5),
            ],
    )
    # Note: The `U_Net` hyperparameters `in_channels` & `out_channels`
    # must correspond to the number of image channels in the dataset's
    # training images, and the number of classes in the dataset
    # (because that's what the model will be trained upon).
    #
    # So there's no point allowing the caller to specify
    # these hyperparameters as command-line arguments.
    model = U_Net(
            in_channels=data.num_channels,
            out_channels=data.num_classes,
            learning_rate=parsed_args.learning_rate,
    )

    logger = TensorBoardLogger(
            save_dir=parsed_args.logger_dirname,
            name=parsed_args.experiment_name)

    if parsed_args.do_short_run:
        # Do a short run, as a quick sanity check.
        trainer = pl.Trainer(
                logger=logger,
                min_epochs=parsed_args.min_epochs,
                max_epochs=parsed_args.num_epochs_short,
                overfit_batches=parsed_args.num_overfit_batches,
                log_every_n_steps=max(parsed_args.num_epochs_short // 10, 1),
        )
    elif parsed_args.do_overfit_batches:
        # Overfit batches.
        trainer = pl.Trainer(
                logger=logger,
                min_epochs=parsed_args.min_epochs,
                max_epochs=parsed_args.num_epochs,
                overfit_batches=parsed_args.num_overfit_batches,
        )
    else:
        # Don't overfit batches; train on ALL batches.
        trainer = pl.Trainer(
                logger=logger,
                min_epochs=parsed_args.min_epochs,
                max_epochs=parsed_args.num_epochs,
        )
    trainer.fit(model, data)


def _add_trainer_specific_args(parser):
    # Configure the trainer.
    group = parser.add_argument_group('train',
            'Configure the PyTorch-Lightning trainer:')

    # Logging
    group.add_argument('--logger-dirname',      default=conf.logger_dirname,
            metavar='subdir')
    group.add_argument('--experiment-name',     default=conf.experiment_name,
            metavar='valid_ident')

    # Training duration
    group.add_argument('--min-epochs',          default=conf.min_epochs,
            metavar='int', type=int)
    group.add_argument('--num-epochs',          default=conf.num_epochs,
            metavar='int', type=int)

    # Overfit batches
    group.add_argument('--num-overfit-batches', default=conf.num_overfit_batches,
            metavar='int', type=int)
    group.add_argument('--do-overfit-batches',  default=conf.do_overfit_batches,
            action='store_true')

    # Short run
    group.add_argument('--num-epochs-short',    default=conf.num_epochs_short,
            metavar='int', type=int)
    group.add_argument('--do-short-run',        default=conf.do_short_run,
            action='store_true')

    # Log progress images
    group.add_argument('--log-progress-images', default=conf.log_progress_images,
            action='store_true')

    return parser


if __name__ == "__main__":
    main()
