"""Hard-coded configuration options for training the U-Net model.

Copyright (c) 2023 James Boyden
"""

#
# Data-module specific configuration options.
#
batch_size = 16
data_dir = 'dataset/'
resize_image_H_W = (200, 200)
num_workers = 4

#
# Model specific configuration options.
#
num_features_per_scale = [64, 128, 256, 512, 1024]  # same as the U-Net paper
learning_rate = 1.0e-2

#
# Training specific configuration options.
#
logger_dirname = 'tb_logs'
experiment_name = 'unet_model_v1'
min_epochs = 1
num_epochs = 1000

# Overfit batches: Speed-run the training process on just a few batches:
#  https://lightning.ai/docs/pytorch/stable/common/trainer.html#overfit-batches
#
# If you can't overfit your model on just a few batches,
# there might be a problem with your model.
num_overfit_batches = 2  # How many batches to overfit.
do_overfit_batches = False  # Attempt to overfit on just a few batches?

# Short run: Sometimes you want to do an extra-short run,
# just for a quick sanity check.  It won't train anything useful,
# but you can verify that the loss plots are moving downwards.
num_epochs_short = 20
do_short_run = False

# Log an image grid of input images and prediction results.
log_progress_images = False
