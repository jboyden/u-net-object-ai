"""A Lightning module that implements a U-Net model for image segmentation.

Python classes (Lightning modules) defined within this Python module:

- :class:`U_Net`

Copyright (c) 2023 James Boyden
"""

import torch
import torchmetrics
import pytorch_lightning as pl

from torch import nn, optim
from torch import cat as torch_cat
from torchvision.utils import make_grid
from typing import Sequence

import conf

from cmdline import parse_list, parse_positive_int
from submodules import U_NetDoubleConv


class U_Net(pl.LightningModule):
    """This Lightning module implements a U-Net model.

    In this implementation of a U-Net model, we made 5 significant deviations
    from the original U-Net paper:

    1. In contrast to the fixed input resolution of 572 x 572 pixels specified
      in the original U-Net paper, this implementation accepts images of an
      arbitrary (height, width) input resolution.

    2. In contrast to the fixed sequence of num-features-per-level specified
      in the original U-Net paper (specifically: [64, 128, 256, 512, 1024]),
      this implementation allows client code to specify any number of levels,
      and any number of features per level.

    3. In submodule :class:`U_NetDoubleConv`:
      Rather than the 3x3 "valid convolutions" that shave 1px off the border
      of the convolution result (resulting in a progressive reduction of the
      (height, width) resolution of each successive feature map), we instead
      use 1px-padded 3x3 "same convolutions" that maintain the resolution.
      This avoids the need to crop "skip connections" before concatenation;
      and also avoids the need to tile multiple less-than-input-image-size
      segmentation results.

    4. In submodule :class:`U_NetDoubleConv`:
      We apply batch normalisation after the 3x3 convolution, before the ReLU.
      [And then because of the batch normalisation step, there's no benefit
      to adding a bias after the convolution step, because any bias would be
      negated by the batch normalisation anyway.  So we specify ``bias=False``
      to the ``nn.Conv2d`` constructor.]

    5. Rather than exactly 2 channels of output by the final convolution layer
      (for a per-pixel multiclass classifier of exactly 2 classes), the final
      convolution layer in this implementation allows any positive number of
      output channels (for any number of output classes), even including just
      1 channel (for a per-pixel binary classifier).
    """

    @staticmethod
    def add_model_specific_args(parser):
        # Hyperparameters for the U-Net model implementation.
        group = parser.add_argument_group('model',
                'Hyperparameters for the U-Net model:')

        # Note: The `U_Net` hyperparameters `in_channels` & `out_channels`
        # must correspond to the number of image channels in the dataset's
        # training images, and the number of classes in the dataset
        # (because that's what the model will be trained upon).
        #
        # So there's no point allowing the caller to specify
        # these hyperparameters as command-line arguments.
        group.add_argument('--num-features-per-scale', metavar='int,int,...',
                action=parse_list(parse_positive_int, min_len=1),
                default=conf.num_features_per_scale)
        group.add_argument('--learning-rate', metavar='float', type=float,
                default=conf.learning_rate)

        return parser


    def __init__(self,
            in_channels: int=3,
            out_channels: int=2,
            num_features_per_scale=[64, 128, 256, 512, 1024],
            *,
            learning_rate: float=conf.learning_rate,

            # Quoting section 3 ("Training") of the U-Net paper:
            #   "The energy function is computed by a pixel-wise soft-max
            #   over the final feature map combined with the cross entropy
            #   loss function."
            #
            # Note that Torch's `nn.CrossEntropyLoss` assumes logits
            # ("unnormalized logits for each class") as the loss input:
            #  https://pytorch.org/docs/stable/generated/torch.nn.CrossEntropyLoss.html
            multi_loss_fn=nn.CrossEntropyLoss,

            # For binary classification, we use Torch's `nn.BCEWithLogitsLoss`:
            #  https://pytorch.org/docs/stable/generated/torch.nn.BCEWithLogitsLoss.html
            # which expects logits as input (similar to `nn.CrossEntropyLoss`,
            # which we're using for multiclass classification), in contrast to
            # `nn.BCELoss.html`, which expects input probabilities in (0,1):
            #  https://pytorch.org/docs/stable/generated/torch.nn.BCELoss.html
            binary_loss_fn=nn.BCEWithLogitsLoss,
    ):
        """Construct a U-Net with specified architectural hyperparameters.

        Args:
            in_channels (positive int):
                defaults to ``3``, expecting an RGB image
            out_channels (positive int):
                defaults to ``2``, for 2-class multiclass classification
            num_features_per_scale (non-empty sequence of positive int):
                defaults to ``[64, 128, 256, 512, 1024]``, like the U-Net paper
            learning_rate (float):
                defaults to `1.0e-2`
            multi_loss_fn (loss function to use for multi-class):
                defaults to ``nn.CrossEntropyLoss``
            binary_loss_fn (loss function to use for binary-class):
                defaults to ``nn.BCEWithLogitsLoss``

        `num_features_per_scale` is a non-empty sequence of positive integers,
        one element per level of the U-Net.  The integer for each level is the
        desired number of features to exist by the end of that level, computed
        by the 2 blue-arrow convolutions within that level.
          
        The default list argument corresponds to the architecture illustrated
        in the original U-Net paper.

        [Note: In the architecture illustrated in the original U-Net paper,
        the number of features at each level doubles at the first rightward
        blue arrow on that level of the contracting path (left side);
        and then halves again at the first rightward blue arrow on that level
        of the expanding path (right side).
          
        But strictly speaking, this doubling/halving of features is simply a
        design choice in the original U-Net paper:  It's not required by any
        innate mathematical characteristic of the network or the convolutions
        (which are, after all, simply matrix multiplications from an "input"
        number of dimensions to an "output" number of dimensions).

        What *is* required by the network:

          - (1) The **number of levels** on the contracting path (left side)
            **equals** the number of levels on the expanding path (right side),
            so that the skip connections match up.
          - (2) The **image resolution halves** from level to level
            on the contracting path due to the 2x2 max-pooling.
          - (3) The **image resolution doubles** from level to level
            on the expanding path due to 2x2 transposed convolutions.

        This U-Net implementation assumes requirements (1), (2), and (3);
        and implements accordingly.

        [And, although *also* not strictly necessary, this implementation also
        assumes that the **number of features** at each level **matches** on
        the left & right sides of the "U" shape.  It's not strictly necessary
        because the skip connections in the U-Net only require feature-matrix
        concatenation, rather than any element-wise feature-matrix operations.]

        This implementation allows client code to specify any number of levels,
        and any number of features at each level.
        """
        super().__init__()
        # Save all hyperparameters for easy re-loading:
        #  https://lightning.ai/docs/pytorch/stable/common/checkpointing_basic.html#save-hyperparameters
        #
        # Fun fact: PyTorch-Lightning actually defines "hyperparameters"
        # as the arguments to a model's `__init__` method.
        #
        # So the hyperparameters that will be saved by the following
        # method-call are the arguments to this `__init__` method.
        # The hyperparametrs will be saved in some `*/*/*/hparams.yaml` file.
        self.save_hyperparameters()

        # Set up the appropriate loss function.
        self.loss_fn = (
                # multi-class classification:
                multi_loss_fn if out_channels >= 2
                # else, binary classification:
                else binary_loss_fn)

        if isinstance(self.loss_fn, type):
            # It's an unconstructed type.  So construct it!
            self.loss_fn = self.loss_fn()

        # Set up the Learning Rate.
        self.learning_rate = learning_rate

        # Install functions to log the accuracy & f1-score.
        self.num_classes = (out_channels if out_channels >= 2 else 2)
        self.accuracy = torchmetrics.Accuracy(
                task="multiclass",
                num_classes=self.num_classes,
        )
        #self.f1_score = torchmetrics.F1Score(
        #        task="multiclass",
        #        num_classes=self.num_classes,
        #)

        # Construct the layers in the U-Net architecture.
        self._construct_architecture(in_channels, out_channels,
            num_features_per_scale)

        # This determines whether prediction images will be saved
        # in method `training_step`.
        self.log_progress_images = False


    def _construct_architecture(self, in_channels, out_channels,
            num_features_per_scale):
        # What's the difference between `nn.ModuleList` and `nn.Sequential`?
        #
        # `nn.Sequential` is a chained sequence of modules, encapsulated in a
        # module that, when instructed to process itself (e.g. with `forward`),
        # processes the modules in the sequence, chaining outputs to inputs.
        #
        # In contrast, `nn.ModuleList` is simply a container of modules
        # (in which the contained modules are all properly registered, as if
        # they were each contained directly as an attribute of the class);
        # but the modules are not connected for processing.
        #
        # More info in the docs:
        #  https://pytorch.org/docs/stable/generated/torch.nn.Sequential.html
        #  https://pytorch.org/docs/stable/generated/torch.nn.ModuleList.html
        self.contracting_path_convs = nn.ModuleList()
        self.exp_path_blue_arrows = nn.ModuleList()
        self.exp_path_green_arrows = nn.ModuleList()

        # This `nn.MaxPool2d(kernel_size=2, stride=2)` halves the resolution
        # in each image dimension of (rows, columns) of pixels, to step from
        # one U-Net level down to the level below.
        #
        # Note that we only need to instantiate a single `MaxPool2d` instance,
        # which will be used for *all* max-pooling within this module, because
        # `MaxPool2d` is non-parametric (non-learning) and may thus be re-used
        # any number of times.
        #
        # Note also that this `stride=2` max-pooling operation will perform
        # a flooring integer-division-by-2 in the (height, width) dimensions
        # of the image resolution.  So the output image resolution from this
        # max-pooling operation will be "half or less" the input resolution
        # in each dimension.  So doubling the resolution of the output image
        # afterwards is NOT guaranteed to produce a resolution equal to that
        # of the input image.
        self.downscale_pool = nn.MaxPool2d(kernel_size=2, stride=2)

        # In this loop, we instatiate the double-blue arrows at each scale
        # on the contracting path (left side) of the architecture diagram in
        # the original U-Net paper.
        # 
        # Note that we're not *connecting* these modules together in this loop:
        # We're simply creating them and storing them in a list for later use
        # in the `forward` method.
        for num_features in num_features_per_scale[:-1]:
            self.contracting_path_convs.append(
                    U_NetDoubleConv(in_channels, num_features))
            in_channels = num_features

        # This is the bottom-most level along the "base" of the U-Net.
        # We store this `U_NetDoubleConv` as its own attribute, separately
        # from `self.contracting_path_convs`, because in the `forward` method,
        # this "base" level does not produce "skip connections"; nor is its
        # result downscaled by max-pool.
        #
        # Because `num_features_per_scale` is guaranteed to be non-empty,
        # `num_features_per_scale[-1]` is guaranteed to be a valid index.
        self.base_level_of_unet = \
                U_NetDoubleConv(in_channels, num_features_per_scale[-1])

        # In this loop, we instantiate the expanding path (right side)
        # of the architecture diagram in the original U-Net paper:
        # both the rightward-pointing double-blue arrows at each scale
        # and the upward-pointing green arrows between scales (levels).
        #
        # We construct from the base level of the "U" shape,
        # upwards and to the right (in the order that processing will occur);
        # but again, note that we're not *connecting* these modules together
        # for processing in this loop.
        #
        # For example, if:
        #
        #   >>> num_per_scale = [64, 128, 256, 512, 1024]
        #
        # then:
        #
        #   >>> num_per_scale[1:] == [128, 256, 512, 1024]
        #   True
        #   >>> num_per_scale[:-1] == [64, 128, 256, 512]
        #   True
        #   >>> list(zip(num_per_scale[1:], num_per_scale[:-1]))
        #   [(128, 64), (256, 128), (512, 256), (1024, 512)]
        #   >>> list(reversed(list(zip(num_per_scale[1:], num_per_scale[:-1]))))
        #   [(1024, 512), (512, 256), (256, 128), (128, 64)]
        #
        # This awkward code is because `zip` objects are not reversible.
        for (num_features_before, num_features_after) in reversed(list(zip(
                num_features_per_scale[1:],  # features before RHS upscale
                num_features_per_scale[:-1]  # features after RHS upscale
        ))):
            # First, the up-pointing green arrows for upscaling between levels.
            #
            # The first argument is `num_features*2` because we're upscaling
            # from a higher-feature-count lower scale to a lower-feature-count
            # higher scale on the right side (expanding path) of the U-Net.
            assert num_features_before > num_features_after
            self.exp_path_green_arrows.append(
                    nn.ConvTranspose2d(
                            num_features_before, num_features_after,
                            kernel_size=2, stride=2,
                    )
            )
            # Now we use `(num_features_after * 2)` because we've concatenated
            # the feature-matrix of the skip connection.
            #
            # [It may be that `(num_features_after * 2 == num_features_before)`
            # (if the specified number of features doubles from level to level
            # on the contracting path, as in the original U-Net paper),
            # but it's not strictly required.]
            self.exp_path_blue_arrows.append(
                    U_NetDoubleConv(num_features_after * 2, num_features_after))

        # Verify that before each double-blue arrow on the expanding path,
        # there is an up-pointing green arrow for upscaling image resolution
        # between levels.
        #
        # [This is an implementation-correctness check, not an argument check.]
        assert len(self.exp_path_green_arrows) == len(self.exp_path_blue_arrows)

        # Now we can combine the matching (green arrows, double-blue arrows)
        # into the "expanding path", stored as a `list` for repeat iteration:
        self.expanding_path_convs = list(zip(
                self.exp_path_green_arrows,
                self.exp_path_blue_arrows))

        # Verify that the number of levels on the contracting path equals
        # the number of levels on the expanding path.
        #
        # [This is an implementation-correctness check, not an argument check.]
        assert len(self.contracting_path_convs) == len(self.expanding_path_convs)
        # Now a final 1x1 conv to produce the per-pixel classification logits.
        # It doesn't change the resolution (height, width) of the layers at the
        # top of the right side (expanding path); it only changes the number of
        # features so that we have a classifier at the end of the network.
        #
        # Because `num_features_per_scale` is guaranteed to be non-empty,
        # `num_features_per_scale[0]` is guaranteed to be a valid index.
        self.final_conv = nn.Conv2d(
                num_features_per_scale[0], out_channels,
                kernel_size=1)


    def forward(self, x):
        # We'll store the corresponding per-level skip connections.
        skip_connections = []

        # Down the contracting path (left side of the "U" shape).
        # The contracting path downscales the image resolution and
        # increases the number of features.
        for blue_arrows_to_increase in self.contracting_path_convs:
            # Increase number of features, maintain input image resolution:
            x = blue_arrows_to_increase(x)
            skip_connections.append(x)
            # The downscaling red arrow at the end of the current level.
            # Downscale image resolution:
            x = self.downscale_pool(x)

        # Across the "base" level of the U-Net.
        x = self.base_level_of_unet(x)

        # Reverse the corresponding per-level skip connections
        # (which were appended in order of decreasing resolution)
        # to be now in order of increasing resolution.
        skip_connections = skip_connections[::-1]

        # Now back up the expanding path (right side of the "U" shape).
        for (skip_conn, (green_arrow_to_upscale, blue_arrows_to_reduce)) \
                in zip(skip_connections, self.expanding_path_convs):

            x = green_arrow_to_upscale(x)

            # NOTE: Even using 1px-padded 3x3 "same convolutions" that maintain
            # the same image resolution, the computed `skip_conn` at this level
            # is NOT guaranteed to have the same resolution as the upscaled `x`
            # at this level, due to the flooring integer-division-by-2 in each
            # dimension by the max-pooling operation `self.downscale_pool(x)`.
            #
            # [For more information, consult the comment before the definition
            # of `self.downscale_pool` in the `__init__` method.]
            #
            # Torch function `torch.cat` states:
            #  https://pytorch.org/docs/stable/generated/torch.cat.html
            #   """
            #   All tensors must either have the same shape
            #   (except in the concatenating dimension) or be empty.
            #   """
            #
            # So before we attempt to concatenate `skip_conn` and `x`, we must
            # ensure that they have the same shape.
            if x.shape != skip_conn.shape:
                # Oh no, our worst fears [see footnote] have been confirmed!
                # 
                # [footnote] "Our worst fears" that occur with >=75% probability
                # for images of an arbitrary (height, width) input resolution.
                #
                # However will we handle this entirely unforseen (>=75%) event??
                #
                # First, we assume that some dimension of `x` is smaller
                # than the corresponding dimension of `skip_conn`, due to
                # a flooring integer-division-by-2 max-pooling downscale
                # (after `skip_conn` was produced) in the contracting path.
                #
                # Secondly, we assume that if we've repaired this problem as
                # we've encountered it at each level, the shape discrepancy
                # will not be more than 1px in each dimension.
                #
                # Let's first verify these assumptions to ensure we're on
                # the right track.  Assume a U-Net input shape:
                #   `(batch_size, img_channels, img_height, img_width)`
                # that becomes an internal feature-map shape:
                #   `(batch_size, num_features, img_height, img_width)`.
                # We want to extract the (height, width) for comparison.
                (_sk_bsize, _sk_nfeats, sk_height, sk_width) = skip_conn.shape
                ( _x_bsize,  _x_nfeats,  x_height,  x_width) = x.shape
                # Why are we in this if-statement block?
                # Verify that at least one of the dimensions of `x` is smaller.
                x_height_needs_padding = (sk_height > x_height)
                x_width_needs_padding = (sk_width > x_width)
                assert (x_height_needs_padding or x_width_needs_padding)
                # We've verified that there's a shape discrepancy.
                # Verify that the shape discrepancy is not more than 1px.
                assert (sk_height == x_height or
                        sk_height == x_height + 1)
                assert (sk_width == x_width or
                        sk_width == x_width + 1)

                # Now,
                # 1. Do we want to pad `x` to the shape of `skip_conn`?
                # 2. Do we want to raise an exception and blame the client code
                #    that supplied an us input image that was not a power-of-2
                #    in its input (height, width) resolution?
                # 3. Do we want to image-resize `x` to have the same shape
                #    as `skip_conn` (eg, using bilinear interpolation)?
                #
                # Option #3 sounds expensive to compute (especially over
                # so many feature channels).  Option #2 seems a little unkind
                # towards our mathematically-challenged client, who just wants
                # to segment their image please.
                #
                # So, Option #1 it is...  I think it's reasonable to pad `x`
                # with an extra row and/or extra column of pixels as needed,
                # by duplicating the current boundary pixels.
                #
                # Normally in this situation, I'd use `numpy.empty_like`,
                # then initialise the memory manually using ndarray slicing.
                # But I want to maintain the Pytorch autograd graph.
                # So instead, I'll use `torch.cat`.
                if x_height_needs_padding:
                    # eg, if `(sk_height == 5)` but `(x_height == 4)`,
                    # we want to duplicate the row at `[x_height=3]`.
                    # To maintain the same `ndim` (number of dimensions),
                    # we'll use  `[x_height - 1:]`.
                    # The following slicing assumes a shape of:
                    #   `(batch_size, num_features, width, width)`.
                    padding_for_x_height = x[:, :, x_height - 1:, :]
                    # Use `dim=2` to concatenate along the "height" dimension.
                    x = torch_cat((x, padding_for_x_height), dim=2)

                if x_width_needs_padding:
                    # eg, if `(sk_width == 9)` but `(x_width == 8)`,
                    # we want to duplicate the row at `[x_width=7]`.
                    # To maintain the same `ndim` (number of dimensions),
                    # we'll use  `[x_width - 1:]`.
                    # The following slicing assumes a shape of:
                    #   `(batch_size, num_features, width, width)`.
                    padding_for_x_width = x[:, :, :, x_width - 1:]
                    # Use `dim=3` to concatenate along the "width" dimension.
                    x = torch_cat((x, padding_for_x_width), dim=3)

            # Now we can be confident we can concatenate `skip_conn` and `x`.
            # Use `dim=1` to concatenate along the "features" dimension.
            skip_concat_x = torch_cat((skip_conn, x), dim=1)
            x = blue_arrows_to_reduce(skip_concat_x)

        # Now a final 1x1 conv to produce the per-pixel classification logits.
        return self.final_conv(x)


    def _calc_one_step_loss(self, batch, batch_idx):
        (x, targets) = batch
        logits = self.forward(x)  # equivalent to: logits = self(x)
        #loss = F.cross_entropy(logits, targets)
        loss = self.loss_fn(logits, targets)
        return (targets, logits, loss)


    def training_step(self, batch, batch_idx):
        (targets, logits, loss) = self._calc_one_step_loss(batch, batch_idx)

        if self.log_progress_images:
            self._imsave_predictions(batch, batch_idx, logits)

        accuracy = self.accuracy(logits, targets)
        #f1_score = self.f1_score(logits, targets)
        self.log_dict({
                        'train_loss': loss,
                        'train_acc': accuracy,
                        #'train_f1': f1_score
                },
                on_step=False, on_epoch=True, prog_bar=True)
        return loss


    def _imsave_predictions(self, batch, batch_idx, logits):
        ident = 'e%04d_b%d' % (self.trainer.current_epoch, batch_idx)

        (images, targets) = batch
        # The images dimensions should be: (batch, channels, height, width)
        # The targets dimensions should be: (batch, classes, height, width)
        # The logits dimensions should be: (batch, classes, height, width)
        assert (images.shape[0] == logits.shape[0])
        assert (images.shape[2] == logits.shape[2])
        assert (images.shape[3] == logits.shape[3])
        assert (targets.shape == logits.shape)

        preds = torch.argmax(logits, dim=1)
        (B, H, W) = preds.shape
        preds = preds.reshape((B, 1, H, W))

        logger = self.logger.experiment
        logger.add_image(ident + '_images', make_grid(images))
        logger.add_image(ident + '_preds', make_grid(preds))


    def validation_step(self, batch, batch_idx):
        (targets, logits, loss) = self._calc_one_step_loss(batch, batch_idx)
        self.log('val_loss', loss)
        return loss


    def test_step(self, batch, batch_idx):
        (targets, logits, loss) = self._calc_one_step_loss(batch, batch_idx)
        self.log('test_loss', loss)
        return loss


    def predict_step(self, batch, batch_idx):
        (x, targets) = batch
        logits = self.forward(x)  # equivalent to: logits = self(x)
        preds = torch.argmax(logits, dim=1)
        return preds


    def configure_optimizers(self):
        # This is also where we would use a learning-rate scheduler.
        return optim.Adam(self.parameters(), lr=self.learning_rate)

