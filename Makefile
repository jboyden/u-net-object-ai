install-train-deps:
	python3 -m pip install -r requirements-train-deps.txt

HOSTNAME = $(shell hostname)
TIMESTAMP = $(shell date '+%Y-%m-%d--%H-%M-%S')

make-snapshot:
	tar cjvf /tmp/u-net-snapshot--$(HOSTNAME)--$(TIMESTAMP).tar.bz2 u_net
